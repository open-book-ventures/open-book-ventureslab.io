[![pipeline status](https://gitlab.com/open-book-ventures/open-book-ventures.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/open-book-ventures/open-book-ventures.gitlab.io/-/commits/master)

---

The website for Open Book Ventures.

Built using GitLab Pages and Hugo. http://open-book-ventures.gitlab.io

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
  - [Preview your site](#preview-your-site)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.
